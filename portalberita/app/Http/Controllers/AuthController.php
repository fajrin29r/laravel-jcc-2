<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function dashboard()
    {
    	return view('halaman2.register');
    }

    public function sdatang(Request $request)
    {
    	// dd($request->all());
    	$fname = $request['fname'];
    	$lname = $request['lname'];

    	return view('halaman2.sdatang', compact('fname','lname'));
    }
}
