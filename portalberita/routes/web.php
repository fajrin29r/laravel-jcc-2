<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/data-tables', function(){
	return view('halaman.data-table');
});

Route::get('/tables', function(){
	return view('halaman.table');
});

// Route::get('/master', function(){
// 	return view('layout.master');
// });

Route::get('/', 'IndexController@dashboard');
Route::get('/pendaftaran', 'FormController@pendataan');

Route::post('/kirim', 'FormController@welcome');

// Route::get('/', 'HomeController@dashboard');
// Route::get('/register', 'AuthController@dashboard');
// Route::post('/welcome', 'AuthController@sdatang');

//CRUD Kategori
//mengarah ke form create data
Route::get('/cast/create', 'CastController@create');
//menyimpan data ke table cast
Route::post('/cast', 'CastController@store');

//tampil semua data
Route::get('/cast', 'CastController@index');
//detail data
Route::get('/cast/{cast_id}', 'CastController@show');

//mengarah ke form edit data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

//update data ke table cast
Route::put('/cast/{cast_id}', 'CastController@update');

//delete data
Route::delete('/cast/{cast_id}', 'CastController@destroy');