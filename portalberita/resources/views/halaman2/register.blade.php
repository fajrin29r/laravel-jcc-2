<!DOCTYPE html>
<html>
<head>
	<title>Document</title>
</head>
<body>
	<h1>Buat Account Baru</h1>
	<h2>Sign Up Form</h2>
	<form action="/welcome" method="post">
		@csrf
        <label>First name :</label><br><br>
		<input type="text" name="fname"><br><br>
		<label>Last name :</label><br><br>
		<input type="text" name="lname"><br><br>
		<label>Gender</label><br><br>
		<input type="radio">Man<br>
		<input type="radio">Women<br>
		<input type="radio">Other<br><br>
		<label>Nationality</label><br><br>
		<select name="country">
			<option value="Indonesia">Indonesia</option>
			<option value="Canada">Canada</option>
			<option value="Korea Selatan">Korea Selatan</option>
			<option value="Inggris">Inggris</option>
		</select><br><br>
		<label>Language Spoken</label><br><br>
		<input type="checkbox" name="Language">Bahasa Indonesia<br>
		<input type="checkbox" name="Language">English<br>
		<input type="checkbox" name="Language">Arabic<br>
		<input type="checkbox" name="Language">Japanese<br><br>
		<label>Bio</label><br><br>
		<textarea name="message" rows="10" cols="30"></textarea><br><br>
	
		<input type="submit" name="submit" value="Sign Up">
	</form>
</body>
</html>