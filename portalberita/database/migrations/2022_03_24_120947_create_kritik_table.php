<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKritikTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kritik', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id'); //tipe data sesuai dengan id table user (data harus positif)
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); // menghapus parent akan menghapus childnya
            $table->unsignedBigInteger('film_id'); //tipe data sesuai dengan id table user (data harus positif)
            $table->foreign('film_id')->references('id')->on('film')->onDelete('cascade'); // menghapus parent akan menghapus childnya
            $table->text('content');
            $table->integer('point');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kritik');
    }
}
